<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;

class LogController extends Controller
{
    public function index(){
        $data['history'] = Log::all('city', 'info');
        return $data;
    }
}
