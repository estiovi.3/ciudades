<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CityController extends Controller
{
    public function getInfo(Request $request){
        $response = [];
        $response['news'] = $this->news($request->city);
        $response['current_weather'] = $this->weather($request->city);
        return $response;
    }

    private function news($city){
        $url = env('NEWS_API_URL').'?q='.$city.'&apiKey='.env('NEWS_API_KEY');
        return Http::get($url)['articles'];
    }

    private function weather($city){
        $url = env('WEATHER_API_URL').'?q='.$city.'&appid='.env('WEATHER_API_KEY');
        $response = Http::get($url);
        return $response->json();
    }
}
