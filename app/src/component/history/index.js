import React, { useEffect, useState } from 'react';

import axios from 'axios';
import { Table, TableContainer, TableHead, TableCell, TableBody, TableRow } from '@mui/material';

import { URL_GET_HISTORY } from '../constants';
/*
const [history, setHistory] = useState({
    history: '',
})
*/
const History = () => {

    const [data, setData] = useState({history:[]});

    const getData = async () => {
        await axios.get(URL_GET_HISTORY)
            .then(res => {
                res.data.history.map( h => ( console.log(h.city) ));
                setData(res.data);
            })
    }

    useEffect(() => {
        getData();
    }, [])

    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>City</TableCell>
                        <TableCell>Info</TableCell>
                    </TableRow>
                </TableHead>

                <TableBody>
                    {
                        data.history.map(h => (
                            <TableRow>
                                <TableCell>{h.city}</TableCell>
                                <TableCell>{h.info}</TableCell>
                            </TableRow>
                        ))
                    }
                </TableBody>
            </Table>
        </TableContainer>
    );

}
/*
{
    data.history.map(h => (
        <TableRow>
            <TableCell>{h.city}</TableCell>
            <TableCell>{h.info}</TableCell>
        </TableRow>
    ))
}
*/
export default History;