import React, { useState } from 'react';
import axios from 'axios';
import { URL_GET_CITIES } from '../constants';
import { styled } from '@mui/material/styles';
import { TextField, Box, Button, Card, CardActions, CardContent, Typography, Paper, Grid, Link   } from '@mui/material';

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const bull = (
    <Box
        component="span"
        sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
    >
        •
    </Box>
);

const City = () => {
    
    const [ciudad, setCiudad] = useState([]);
    const [data, setData] = useState(
        { 
            news: [], 
            current_weather: {}
        }
    );

    const getData = async () => {
        await axios.get(URL_GET_CITIES + ciudad)
            .then(res => {
                console.log(res.data);
                setData(res.data);
            })

    }

    const consultar = () =>{
        getData();
    }

    const handleCiudadValue = e =>{
        const { name, value } = e.target;
        setCiudad(value);
    }

    return (
        <div>
            <h2>Consultar Informacion por Ciudad</h2>
            <Box
                component="form"
                sx={{
                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="off"
            >
                <TextField name='ciudad' onChange={handleCiudadValue} variant="standard" />
                <Button variant="contained" onClick={consultar}>Buscar</Button>
                <br></br>
                <h1>News</h1>
                <Box sx={{ flexGrow: 1 }}>
                    <Grid container spacing={2}>
                        { data.news.map( n => (

                            <Grid item xs={3}>
                                <Item>
                                    <Card sx={{ minWidth: 275 }}>
                                        <CardContent>
                                            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                                                {n.source.name}
                                            </Typography>
                                            <Typography variant="h5" component="div">
                                                {n.title}
                                            </Typography>
                                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                                {n.author}{bull}{n.publishedAt}
                                            </Typography>
                                            <Typography variant="body2">
                                                {n.description}
                                            </Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Link href={n.url}>View More</Link>
                                        </CardActions>
                                    </Card>
                                </Item>
                            </Grid>

                        ) ) }
                    </Grid>
                </Box>
                <h1>
                    Wheater
                </h1>
                
                    <Card sx={{ minWidth: 275 }}>
                        <CardContent>
                            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                            {data.current_weather.visibility}
                            </Typography>
                            <Typography variant="h5" component="div">
                            {data.current_weather.weather.map(w => ( <div>{w.main}</div> ))}
                            </Typography>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                            Temp {bull} {data.current_weather.main.temp} Feel Like {bull} {data.current_weather.main.feels_like}
                            </Typography>
                        </CardContent>
                    </Card>
            </Box>
        </div>
    );

}
//{data.current_weather.weather.map(w => ( <div>{w.description}</div> ))}
export default City;